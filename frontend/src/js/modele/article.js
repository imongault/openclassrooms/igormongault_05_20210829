/** Classe representant un Produit
 *   et la liste des options de personnalisation */

 import Produit from "./produit";

 class Article {
 
     constructor(theArticle){
         this.colors = theArticle.colors;
         this.produit = new Produit(theArticle._id,theArticle.name,theArticle.price,theArticle.description,theArticle.imageUrl)
     }
 
     async getColors(){
         return  await this.colors;
     }
 
     async getProduct(){
         return  this.produit;
     }
 
 }
 
 export default Article;