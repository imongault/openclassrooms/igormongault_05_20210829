class Produit {

    constructor(id,name,price,description,imageUrl){
           this._id = id;
           this.name = name;
           this.price = price;
           this.description = description;
           this.imageUrl = imageUrl;
    }

    async getPrice(){
        return await this.price /100;
    }

}

export default Produit;