// Methode pour ajouter un produit dans le panier

export const ajouterAuPanier = async (produit) => {
    if (produit == undefined) {
      return 0;
    }
  
    let lePanier = await recupererPanier();
  
    // Verifie si le produit a ajouter
    // figure deja dans la panier
    const eltPanier = await lePanier.find((element) => {
      return element.id == produit.id && element.color == produit.color;
    });
  
    if (lePanier.length == 0 || eltPanier === undefined) {
      lePanier.push({
        id: produit.id,
        name: produit.name,
        urlImage: produit.urlImage,
        quantity: produit.quantity,
        color: produit.color,
        price: produit.price,
      });
    } else {
      eltPanier.quantity =
        parseInt(eltPanier.quantity) + parseInt(produit.quantity);
    }
  
    localStorage.clear();
  
    enregistrerDansPanier(lePanier);
  
    return await totalEltPanier(lePanier);
  };
  
  // Methode pour recuperer les produits du panier
  // se trouvant dans le local storage
  
  export const recupererPanier = async () => {
    if (typeof Storage !== "undefined") {
      let panier = localStorage.getItem("lePanier");
      if (panier == null) {
        return [];
      } else {
        return await JSON.parse(panier);
      }
    } else {
      return [];
    }
  };
  
  /**
   * Methode qui supprime un item du panier
   * @param {identification de l'item a supprimer} id
   * @param {la couleur de l'item a supprimer} color
   * @returns la liste des elements du panier
   */
  export const supprimerEltPanier = async (id, color) => {
    let lePanier = await recupererPanier();
  
    let Panier = lePanier.filter((param) => {
      if (param.id == id && param.color == color) {
        return false;
      } else {
        return true;
      }
    });
  
    localStorage.clear();
    enregistrerDansPanier(Panier);
  
    if (Panier.length < lePanier.length) {
      return true;
    } else {
      return true;
    }
  
  };
  
  /**
   * Methode pour enregristrer un element
   * dans le localstorage
   * * */
  export const enregistrerDansPanier = (liste) => {
    localStorage.setItem("lePanier", JSON.stringify(liste));
  };
  
  /**
   * Methode permettant de vider
   * le panier
   */
  
  export const supprimerTout = () => {
    localStorage.clear();
  };
  
  /**
   * Methode pour calculer le nombre d'element
   * du panier
   * * */
  
  export const totalEltPanier = async (lePaniers) => {
    if (lePaniers.length == 0 || !Array.isArray(lePaniers)) {
      return 0;
    } else {
      let qte = 0;
      for (const panier of lePaniers) {
        qte += parseInt(panier.quantity);
      }
  
      return qte;
    }
  };
  
  /**
   * @param {liste des items du panier} liste
   * @returns { le montant total du panier}
   */
  
  export const amountCart = async (liste) => {
    if (liste.length == 0 || !Array.isArray(liste)) {
      return 0;
    } else {
      let total = 0;
      for (const elt of liste) {
        total += elt.quantity * parseInt(elt.price);
      }
  
      return total;
    }
  };
  