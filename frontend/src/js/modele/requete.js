/**
 * Classe contenant des methodes permettant de faire 
 * requete http
 */

 import Article from './article';


 export default class Requete {
 
     // Faire une requete Get et retourner une liste des articles
 
     async get(url){
 
         const reponse = await fetch(url);
         const listArticle = await reponse.json()
         const listeArticle = [];
 
         for(let article of listArticle){
 
             const art = new Article(article);
             listeArticle.push(art);
         }
 
         return  listeArticle;
     }
 
     // Faire une requete Get et retourner  un produit par son Id
 
     async getById(url){
 
         const reponse = await fetch(url);
         const article = await reponse.json()
         const art = new Article(article);
 
         return  art;
     }
 
     // Faire des requte post
     // A revoir
 
 
     async post(url, datas){
         
         const response = await fetch(url,{
               method: 'POST',
               headers:{
                 'Accept':'application/json',
                 'content-Type':'application/json' 
               },
               body: JSON.stringify(datas)
         });
 
         return await response.json();
         
     }
 }
 