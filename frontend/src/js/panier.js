import Requete from "./modele/requete.js";
import Heading from "../components/heading/heading";
import Footer from "../components/footer/footer.js";


import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../components/heading/heading.scss";
import "../components/Pages/paniers.scss";

import "../../node_modules/jquery/dist/jquery.min";
import "../../node_modules/bootstrap/dist/js/bootstrap.bundle.min";
import displayCart from "../components/Pages/affichePanier.js";

const requete = new Requete();
const url = "http://localhost:3000/api/teddies";

try {

    requete.get(url).then((liste) => {
        const head = new Heading();
        head.render(liste);
      });
    
   // Methode pour afficher le contenju du panie + formulaire
    displayCart();
         
    // Ajout du footer
    const footer = new Footer();
    footer.render();
  
} catch (error) {
    console.log(error);
}
