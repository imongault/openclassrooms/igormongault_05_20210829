import * as cartManager from "../../js/modele/cartManager";
import menuDropdown from './dropdownMenu';

export default class Heading {
    render(liste){
      
        const headers = document.querySelector('header.container-fluid');
        const dropdowm = menuDropdown(liste);
        let head = `<nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <div class="container">
              <a href="index.html" class="navbar-brand">Orinoco</a>
               
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarContent" 
              aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
             </button>
            
          <div class="collapse navbar-collapse justify-content-between" id="navbarContent">
          
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="index.html">Accueil</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Produits
               </a>`;
           head += dropdowm;
           head += `</li></ul><form class="form-inline my-2 my-lg-0">
           <a href="panier.html" class="btn btn-success btn-sm ml-3 position-relative">
               <i class="fa fa-shopping-cart"> </i> Panier
               <span class="position-absolute top-0 start-100 translate-middle badge rounded-circle bg-warning" id="prix">0</span>
           </a>
       </form>   
     </div>
   </div>
  </nav>`;

   headers.innerHTML = head;

   cartManager.recupererPanier().then((panier) => {
    cartManager.totalEltPanier(panier).then((total) => {
      document.getElementById("prix").innerHTML = total;
    });
  });

    }
} 


