const menuDropdown =(listeProduits) => {
    
    let menu = "";
    menu += ` <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">`;
    for (const product of listeProduits) {
        
        menu += `<li><a class="dropdown-item" href="produits.html?id=${product.produit._id}">${product.produit.name}</a></li>`;
    }
    menu += `</ul>`;

    return menu;

}

export default menuDropdown;