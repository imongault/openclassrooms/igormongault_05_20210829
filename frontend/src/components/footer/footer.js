export default class Footer{

    render(){
        const footers = document.querySelector('footer');
        let foot =`<div class="container">
        <div class="row">
            <div class="col-md-6 mb-4">
                <h5>À propos</h5>
                <hr class="bg-white mb-2 mt-0 inline-block w-25">
                <p class="mb-0 text-justify w-75">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                  Ipsum suscipit voluptas totam. Ducimus, id autem ullam.
                </p>
            </div>
            <div class="col-md-6">
               <h5>Contact</h5>
               <hr class="bg-white mb-2 mt-0 inline-block  w-25">
               <ul class="list-unstyled">
                 <li>
                   <i class="fa fa-home mr-3"></i>
                   Orinico
                 </li>
                 <li>
                   <i class="fa fa-envelope mr-3"></i>
                   projet5@gmail.com
                 </li>
                 <li>
                   <i class="fa fa-phone mr-3"></i>
                   +1458256398
                 </li>
                 
               </ul>
            </div>
        </div>
     </div>`;

     footers.innerHTML = foot;
    }
}